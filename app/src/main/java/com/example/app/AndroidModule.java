package com.example.app;

import android.content.Context;

import dagger.Provides;

/**
 * Created by raulete on 10/02/14.
 */
public class AndroidModule {

    private final BeginnerApplication application;

    public AndroidModule(BeginnerApplication application) {
        this.application = application;
    }
    /*
    /**
     * Allow the application context to be injected but require that it be annotated with
     * {@link ForApplication @Annotation} to explicitly differentiate it from an activity context.
     */
    @Provides
    @Singleton @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    //@Provides //@Singleton
    //LocationManager provideLocationManager() {
        //return (LocationManager) application.getSystemService(LOCATION_SERVICE);
    //}
}
