package com.example.app;

import android.app.Application;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;

/**
 * Created by raulete on 10/02/14.
 */
public class BeginnerApplication extends Application {

    private ObjectGraph graph;

    @Override public void onCreate(){
        super.onCreate();

        graph = ObjectGraph.create(getModules().toArray());
    }

    protected List<Object> getModules() {
        return Arrays.asList(
                new AndroidModule(this),
                new BeginnerModule()
        );
    }

    public void inject(Object object) {
        graph.inject(object);
    }

}
